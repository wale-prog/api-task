import { test, expect } from '@playwright/test'
import { faker } from '@faker-js/faker'
import { headerWithToken, patchData, credentials, headerWithoutToken, bookingData } from './Utils/HelperMethods';


test.describe('Test api verb like get, post, patch and delete', () => {

  let token;
  let bookingCount;
  let newBookingId;
  const firstName = faker.person.firstName()
 
  test.beforeAll('Obtain the authentication token', async ({ request }) => {
    const response = await request.post('/auth', {
      data: credentials(),
      headers: headerWithoutToken()
    })
    const resp = await response.json()
    token = resp.token

  })

  test('01. Get all the bookings ids and store in a variable ', async ({ request }) => {
    const response = await request.get('/booking')
    const resp = await response.json()
    bookingCount = resp.length
  })

  test('02. Create new booking and store its ID', async ({ request }) => {
    const response = await request.post('/booking', {
      data: bookingData(firstName),
      headers: headerWithoutToken()
    })
    expect(response.status()).toBe(200)
    const resp = await response.json()
    newBookingId = resp.bookingid
  })

  test('03. Get only the created booking by ID', async({request}) => {
    const response = await request.get(`/booking/${newBookingId}`)
    const resp = await response.json()
    expect(resp.firstname).toBe(firstName)
  })

  test('04. Replace some fields in the Created booking with PATCH', async({request}) => {
    const response = await request.patch(`/booking/${newBookingId}`, {
      headers: headerWithToken(token),
      data: patchData("Olawale", "Olutoyin")
    })
    expect(response.status()).toBe(200)
      
    const responseVerify = await request.get(`/booking/${newBookingId}`)
    const respVerify = await responseVerify.json()
    expect(respVerify.firstname).toBe("Olawale")
    expect(respVerify.lastname).toBe("Olutoyin")
  })

  test('05 Delete the created post by ID', async({request}) => {
    const response = await request.delete(`/booking/${newBookingId}`, {
      headers: headerWithToken(token)
    })
    expect(response.status()).toBe(201)
    const responseVerify = await request.get(`/booking/${newBookingId}`)
    expect(responseVerify.status()).toBe(404)
  })

  test('06. Check the number of bookings to ensure integrity', async({ request }) => {
    const response = await request.get('/booking')
    const resp = await response.json()
    expect(bookingCount).toBe(resp.length)
  })
})
