import { faker } from "@faker-js/faker";
import { DateTime } from "luxon";

class HelperMethod {

  credentials = () => {
    const data = { username: "admin", password: "password123" };
    return JSON.stringify(data);
  };

  patchData = (firstName, lastname) => {
    return {"firstname": firstName, "lastname": lastname}
  }

  headerWithoutToken = () => {
    return {
      "Content-Type": "application/json",
      "Accept": "application/json",
    };
  };

  headerWithToken = (token) => {
    return {
      "Content-Type": "application/json",
      "Accept": "application/json",
      "Cookie": `token=${token}`,
    };
  };

  bookingData = (firstName) => {
    const checkinDate = DateTime.now().toFormat("yyyy-MM-dd");
    const checkoutDate = DateTime.now().plus({day:7}).toFormat("yyyy-MM-dd");
    const additionalneeds = ["Breakfast", "Lunch", "Brunch", "Dinner"]
    let randomNeeds = Math.floor(Math.random() * additionalneeds.length);
    const data =  {
      "firstname" : firstName,
      "lastname" : faker.person.lastName(),
      "totalprice" : faker.number.int({min: 100, max: 200}),
      "depositpaid" : true,
      "bookingdates" : {
        "checkin": checkinDate,
        "checkout": checkoutDate,
      },
      "additionalneeds" : additionalneeds[randomNeeds]
    }
    return JSON.stringify(data)
  }
}
export const { headerWithoutToken, patchData, credentials, headerWithToken, bookingData} = new HelperMethod()
